# Jest Practice

## How I Set Up The Project

1. Installed with: `yarn add --dev jest`.
2. Added a `.gitignore` because wow look at all those files in `node_modules/`.
3. Added `src/functions` directory, a `tests` directory within that, and a `sum.js` and `tests/sum.test.js` file to make sure everything is installed and configured properly.
4. Added `"license": "UNLICENSED"` to `package.json` because the `yarn` warning was annoying.
5. Added `"name": "jest-practice"` to `package.json` because it seemed like the right thing to do.

## How I Set Up CI Configuration

I added a `.gitlab-ci.yml` file with that looks like this:

```yaml
image: node:11.6.0

javascript:
  stage: test
  script:
    - yarn install
    - yarn test
```
