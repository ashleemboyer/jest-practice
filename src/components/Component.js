class Component {
  constructor(props) {
    this.props = props || {};
    this.setElement();
  }

  getProp(prop) {
    return this.props[prop];
  }

  render(parent) {
    parent.appendChild(this.element);
  }

  setElement() {
    console.error("TODO: overwrite this createElement");
  }
}

module.exports = Component;
