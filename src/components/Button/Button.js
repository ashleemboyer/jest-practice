const Component = require("../Component");

class Button extends Component {
  constructor(props) {
    super(props);
  }

  setElement() {
    this.element = document.createElement("button");
    this.element.innerHTML = this.getProp("text");
  }

  addEventListener(type, callback) {
    this.element.addEventListener(type, callback);
  }
}

module.exports = Button;
