const Button = require("./Button");

const buttonText = "Click Me!";
const alertText = "I clicked the button!";

beforeAll(() => {
  const button = new Button({ text: buttonText });
  button.render(document.body);
  button.addEventListener("click", () => {
    alert(alertText);
  });
});

describe("components/Button", () => {
  it("renders correctly", () => {
    const buttonElement = document.getElementsByTagName("button")[0];
    expect(buttonElement.innerHTML).toBe(buttonText);
  });

  it("shows an alert when clicked", () => {
    const buttonElement = document.getElementsByTagName("button")[0];
    jest.spyOn(window, "alert").mockImplementation(() => {});
    buttonElement.click();
    expect(window.alert).toBeCalledWith(alertText);
  });
});
